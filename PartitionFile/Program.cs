﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PartitionFile
{
    class Program
    {
        static void Main(string[] args)
        {
            // Verify argument count = 2
            if (args.Length != 2)
            {
                throw new ArgumentException("PartitionFile requires 2 arguments. FilePath and outFileCount, respectively.");
            }

            // Parse outfile count
            int outFileCount;
            if (!int.TryParse(args[1], out outFileCount))
            {
                throw new ArgumentException("Outfile Count " + args[1] + "invalid");
            }
            
            PartitionFile(args[0], outFileCount);

        }

        private static void PartitionFile(string path, int outFileCount)
        {

            // Validate infilepath
            if (!File.Exists(path))
            {
                throw new ArgumentException("Input File " + path + " not found.");
            }

            // Open all outfile for writing
            StreamWriter[] oufs = new StreamWriter[outFileCount];
            string pPath = ParameterizePath(path);
            for (int i = 0; i < outFileCount; i++)
            {
                oufs[i] = new StreamWriter(string.Format(pPath, i));
            }

            // loop along infile, writing lines to files in circular orbit
            int j = 0;
            foreach (string line in File.ReadLines(path))
            {
                oufs[j % outFileCount].Write(line);
                j++;
            }

            // close all outfiles
            foreach (var item in oufs)
            {
                item.Close();
            }

        }

        public static string ParameterizePath(string path)
        {

            // Adds '_{0}' to end of filename, before extension. Used in conjunction with string.format to
            // create specified number of outfiles in an indexed manner.
            return Path.GetDirectoryName(path) + Path.GetFileNameWithoutExtension(path) + "_{0}." + Path.GetExtension(path);

        }
    }
}
